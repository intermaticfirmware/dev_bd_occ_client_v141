//-----------------------------------------------------------------------------
//  All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: uart.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __UART_H__
#define __UART_H__

// Includes
//******************************************************************************

// Public Definitions and Constants
//******************************************************************************
#define TXBUFFSIZE 2000
#define RXBUFFSIZE 2000

#define LOWENERGY_UART 0 // enables LEUART when 1 USART when 0

// Macros - I/O definitions
//******************************************************************************

// Typedefs - Public structs, enums, etc.
//******************************************************************************
typedef struct
{
    uint8_t tx_buf[TXBUFFSIZE];
    int32_t tx_head_point;
    int32_t tx_tail_point;
    int32_t tx_count;
} UART_TX_BUFFER_T;

typedef struct
{
    uint8_t rx_buf[TXBUFFSIZE];
    int32_t rx_head_point;
    int32_t rx_tail_point;
    int32_t rx_count;
} UART_RX_BUFFER_T;


// Global Variable declarations (extern)
//******************************************************************************
extern UART_TX_BUFFER_T g_uart_tx_buffer;
extern UART_RX_BUFFER_T g_uart_rx_buffer;


// Prototype Definitions (extern)
//******************************************************************************
extern void  uart_init(UART_TX_BUFFER_T *p_TxUart_Buffer, UART_RX_BUFFER_T *p_RxUart_Buffer, uint32_t baud_rate);
extern int32_t uart_put_string(uint8_t *buffer, int32_t num_char);
extern uint8_t uart_read_rx_buf (UART_RX_BUFFER_T *p_rx_uart_buffer, uint8_t *len);
extern int32_t uart_rx_data_available(UART_RX_BUFFER_T *p_rx_uart_buffer);

#endif /* UART_H */
