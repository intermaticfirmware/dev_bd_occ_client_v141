//-----------------------------------------------------------------------------
//  All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: uart.c is responsible for initializing and setting HOST MCU
//  UART communications.
//
//-----------------------------------------------------------------------------

//    Include Files
//******************************************************************************
#include "em_cmu.h"
#include "em_gpio.h"
#include "main.h"
#include "hal-config-devbd.h"
#include "uart.h"
#include <stdio.h>
#include <stdarg.h>
#include "em_core.h"
#include "wireless.h"
#include "native_gecko.h"

#if LOWENERGY_UART
#include "em_leuart.h"
#else
#include "em_usart.h"
#endif


//    Constants & Definitions  (File-scope)
//******************************************************************************


//    Variable declarations
//******************************************************************************
UART_TX_BUFFER_T g_uart_tx_buffer;
UART_RX_BUFFER_T g_uart_rx_buffer;


//    Function prototypes
//******************************************************************************
static int uart_read_tx_buf (UART_TX_BUFFER_T *p_uart_buf);
void USART0__RX_IRQHandler(void);
void USART0__TX_IRQHandler(void);


//    Function Implementation
//******************************************************************************

//--------------------------------------------------------------------------------
// NAME      : uart_init
// ABSTRACT  : This function initializes the HOST MCU UART and its buffers.
// ARGUMENTS :
//   Buffers used for UART transmission
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void uart_init(UART_TX_BUFFER_T *p_tx_uart_buffer, UART_RX_BUFFER_T *p_rx_uart_buffer, uint32_t baud_rate)
{
#if LOWENERGY_UART
#define LEUART_INIT                                                                         \
  {                                                                                         \
    leuartEnable,    /* Enable RX/TX when initialization completed. */                      \
    0,               /* Use current configured reference clock for configuring baud rate.*/ \
    9600,            /* 115200 bits/s. */                                                   \
    leuartDatabits8, /* 8 data bits. */                                                     \
    leuartNoParity,  /* No parity. */                                                       \
    leuartStopbits1  /* 1 stop bit. */                                                      \
  }
    LEUART_Init_TypeDef init = LEUART_INIT;
#else
#define USART_INITASYNC                                                                    \
  {                                                                                                \
    usartEnable,           /* Enable RX/TX when initialization is complete. */                     \
    0,                     /* Use current configured reference clock for configuring baud rate. */ \
    115200,                /* 115200 bits/s. */                                                    \
    usartOVS16,            /* 16x oversampling. */                                                 \
    usartDatabits8,        /* 8 data bits. */                                                      \
    usartNoParity,         /* No parity. */                                                        \
    usartStopbits1,        /* 1 stop bit. */                                                       \
    false,                 /* Do not disable majority vote. */                                     \
    false,                 /* Not USART PRS input mode. */                                         \
    0,                     /* PRS channel 0. */                                                    \
    false,                 /* Auto CS functionality enable/disable switch */                       \
    0,                     /* Auto CS Hold cycles */                                               \
    0,                     /* Auto CS Setup cycles */                                              \
    usartHwFlowControlNone /* No HW flow control */                                                \
  }
    USART_InitAsync_TypeDef init = USART_INITASYNC;
#endif
    p_tx_uart_buffer->tx_head_point = 0;
    p_tx_uart_buffer->tx_tail_point = 0;
    p_tx_uart_buffer->tx_count = 0;

    p_rx_uart_buffer->rx_head_point = 0;
    p_rx_uart_buffer->rx_tail_point = 0;
    p_rx_uart_buffer->rx_count = 0;

    // GPIO clock
    CMU_ClockEnable(cmuClock_GPIO, true);

#if LOWENERGY_UART
    // Initialize LEUART0 TX and RX pins
    GPIO_PinModeSet(gpioPortA, 0, gpioModePushPull, 1); // TX
    GPIO_PinModeSet(gpioPortA, 1, gpioModeInput, 0);    // RX

    // Enable LE (low energy) clocks
    CMU_ClockEnable(cmuClock_HFLE, true); // Necessary for accessing LE modules
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO); // Set a reference clock Low Frequency Crystal Osc
    // NOTE: to use at higher baud will require using a diff clock
    // LFXO - 32.768 kHz (reference clock for the LFB clock branch)
    //
    // Enable clocks for LEUART0
    CMU_ClockEnable(cmuClock_LEUART0, true);
    CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_1); // Don't prescale LEUART clock

    // Initialize the LEUART0 module
    LEUART_Init(LEUART0, &init);

    // Enable LEUART0 RX/TX pins on PA[14:12] (see readme.txt for details)
    LEUART0->ROUTEPEN  = LEUART_ROUTEPEN_RXPEN | LEUART_ROUTEPEN_TXPEN;
    LEUART0->ROUTELOC0 = HOST_RXLOC | HOST_TXLOC;
    // Enable LEUART0 RX/TX interrupts
    LEUART_IntEnable(LEUART0, LEUART_IEN_RXDATAV | LEUART_IEN_TXC);
    NVIC_EnableIRQ(LEUART0_IRQn); // tx, rx interrupts
#else
    // Enable oscillator to GPIO and USART0 modules
    CMU_ClockEnable(cmuClock_GPIO, true);
    CMU_ClockEnable(cmuClock_USART0, true);

    // configure UART
    GPIO_PinModeSet( HOST_MCU_UART_PORT, HOST_MCU_TXD, gpioModePushPull, 1 );
    GPIO_PinModeSet( HOST_MCU_UART_PORT, HOST_MCU_RXD, gpioModeInputPull, 0 );

    // Initialize USART asynchronous mode and route pins
    USART_InitAsync(USART0, &init);

    USART0->ROUTEPEN |= USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
    USART0->ROUTELOC0 = USART_ROUTELOC0_RXLOC_LOC0 | USART_ROUTELOC0_TXLOC_LOC0;

    //Initialize USART Interrupts
    USART_IntEnable(USART0, USART_IEN_RXDATAV);
    USART_IntEnable(USART0, USART_IEN_TXC);

    CORE_SetNvicRamTableHandler(USART0_RX_IRQn, (void *)USART0__RX_IRQHandler);
    CORE_SetNvicRamTableHandler(USART0_TX_IRQn, (void *)USART0__TX_IRQHandler);
    NVIC_ClearPendingIRQ(USART0_RX_IRQn);        // Clear pending RX interrupt flag in NVIC
    NVIC_ClearPendingIRQ(USART0_TX_IRQn);        // Clear pending TX interrupt flag in NVIC
    //Enabling USART Interrupts
    NVIC_EnableIRQ(USART0_RX_IRQn);
    NVIC_EnableIRQ(USART0_TX_IRQn);
#endif
}


//--------------------------------------------------------------------------------
// NAME      : uart_put_string
// ABSTRACT  : This function puts a string into the tx buffer
// ARGUMENTS :
//   buffer   - points to the tx buffer
//   num_char - number of bytes adding to the tx buffer
// RETURN    :
//   int     : returns number of characters added to buffer
//--------------------------------------------------------------------------------
int32_t uart_put_string(uint8_t *buffer, int32_t num_char)
{
    uint8_t *p_buffer = buffer;
    uint8_t ch;
    int32_t num_tx;

    UART_TX_BUFFER_T *p_tx_uart_buffer;
    int32_t send_count = 0;

    CORE_DECLARE_IRQ_STATE;

    p_tx_uart_buffer = &g_uart_tx_buffer;

    for ( num_tx = 0; num_tx < num_char; num_tx++)
    {
        CORE_ENTER_ATOMIC();
        /* stop before over running the buffer */
        if (p_tx_uart_buffer->tx_count >= TXBUFFSIZE)
        {
            CORE_EXIT_ATOMIC();
            break;
        }
        ch = *p_buffer & 0xff;
        p_tx_uart_buffer->tx_buf[p_tx_uart_buffer->tx_tail_point] = ch;
        p_tx_uart_buffer->tx_tail_point = (p_tx_uart_buffer->tx_tail_point+1)%TXBUFFSIZE;
        /* start critical section */

        p_tx_uart_buffer->tx_count++;
        /* end of critical section */
        CORE_EXIT_ATOMIC();
        send_count++;
        p_buffer++;
    }
#if LOWENERGY_UART
    /* enable USART interrupt */
    LEUART_IntSet(LEUART0, LEUART_IFS_TXC);
#else
    /* enable USART interrupt */
    USART_IntSet(USART0, USART_IFS_TXC);
#endif
    return (send_count);

}


//--------------------------------------------------------------------------------
// NAME      : uart_read_tx_buf
// ABSTRACT  : This function reads a character from the buffer and puts it into
//             the transmit buffer
// ARGUMENTS :
//   buffer - points to the tx buffer
// RETURN    :
//   int     : returns the number of characters in the buffer
//--------------------------------------------------------------------------------
static int uart_read_tx_buf (UART_TX_BUFFER_T *p_tx_uart_buffer)
{
    // warning called in interrupt routine so no critical section
    if (p_tx_uart_buffer->tx_count == 0)
    {
        return 0;
    }

#if LOWENERGY_UART
    LEUART_Tx(LEUART0, p_tx_uart_buffer->tx_buf[p_tx_uart_buffer->tx_head_point]);
#else
    USART_Tx(USART0, p_tx_uart_buffer->tx_buf[p_tx_uart_buffer->tx_head_point]);
#endif
    p_tx_uart_buffer->tx_head_point = (p_tx_uart_buffer->tx_head_point+1)%TXBUFFSIZE;
    p_tx_uart_buffer->tx_count--;
    return(p_tx_uart_buffer->tx_count);
}


//--------------------------------------------------------------------------------
// NAME      : uart_rx_data_available
// ABSTRACT  : This function returns the number of bytes in the RX buffer.
// ARGUMENTS :
//   buffer - points to the RX buffer
// RETURN    :
//   int - Returns the number of bytes in the RX buffer
//--------------------------------------------------------------------------------
int32_t uart_rx_data_available(UART_RX_BUFFER_T *p_rx_uart_buffer)
{
    int32_t rx_count;
    CORE_DECLARE_IRQ_STATE;

    CORE_ENTER_ATOMIC();   // start critical section
    rx_count = p_rx_uart_buffer->rx_count;
    CORE_EXIT_ATOMIC();    // end critical section
    return rx_count;
}


//--------------------------------------------------------------------------------
// NAME      : uart_read_rx_buf
// ABSTRACT  : This function reads a character from the rx buffer
// ARGUMENTS :
//   buffer - points to the RX buffer
//   len    - pointer to place the number of bytes read
// RETURN    :
//   int - returns the number of characters still left in the buffer
//--------------------------------------------------------------------------------
uint8_t uart_read_rx_buf (UART_RX_BUFFER_T *p_rx_uart_buffer, uint8_t *len)
{
    uint8_t read_char;
    CORE_DECLARE_IRQ_STATE;


    CORE_ENTER_ATOMIC();    // start critical section
    if (p_rx_uart_buffer->rx_count == 0)
    {
        *len = 0;
        CORE_EXIT_ATOMIC();  // end of critical section
        return 0;
    }

    read_char = p_rx_uart_buffer->rx_buf[p_rx_uart_buffer->rx_head_point];
    p_rx_uart_buffer->rx_head_point = (p_rx_uart_buffer->rx_head_point+1)%RXBUFFSIZE;

    p_rx_uart_buffer->rx_count--;
    *len = 1;
    CORE_EXIT_ATOMIC();  // end of critical section
    return(read_char);
}


#if LOWENERGY_UART
//--------------------------------------------------------------------------------
// NAME      : LEUART0_IRQHandler
// ABSTRACT  : Interrupt handler for transmitting on UART
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void LEUART0_IRQHandler(void)
{
    uint8_t rx_char;

    // Note: These are static because the handler will exit/enter
    //       multiple times to fully transmit a message.
    UART_RX_BUFFER_T *p_rx_uart_buffer = &g_uart_rx_buffer;
    UART_TX_BUFFER_T *p_tx_uart_buffer = &g_uart_tx_buffer;

    // Acknowledge the interrupt
    uint32_t flags = LEUART_IntGet(LEUART0);
    LEUART_IntClear(LEUART0, flags);

    // RX portion of the interrupt handler
    if (flags & LEUART_IF_RXDATAV)
    {
        // While there is still incoming data
        while (LEUART0->STATUS & LEUART_STATUS_RXDATAV)
        {
            rx_char = LEUART_Rx(LEUART0);

            if (p_rx_uart_buffer->rx_count >= RXBUFFSIZE) // avoid overrun
            {
                break;
            }

            p_rx_uart_buffer->rx_buf[p_rx_uart_buffer->rx_tail_point] = rx_char;
            p_rx_uart_buffer->rx_tail_point = (p_rx_uart_buffer->rx_tail_point+1)%RXBUFFSIZE;

            p_rx_uart_buffer->rx_count++;

            if (p_rx_uart_buffer->rx_count > RXBUFFSIZE) // this will never happen
            {
                p_rx_uart_buffer->rx_head_point = p_rx_uart_buffer->rx_tail_point;
            }
        }
        gecko_external_signal(EX_RX_HOST_MCU_UART);
    }
    // TX portion of the interrupt handler
    if (flags & LEUART_IF_TXC)
    {
        if (uart_read_tx_buf(p_tx_uart_buffer) == 0)
        {
            /* clear USART interrupt */
            LEUART_IntClear(LEUART0, flags);
        }
    }
}
#else
//--------------------------------------------------------------------------------
// NAME      : USART0__RX_IRQHandler
// ABSTRACT  : Interrupt handler for transmitting on UART
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void USART0__RX_IRQHandler(void)
{
    uint32_t flags;
    UART_RX_BUFFER_T *p_rx_uart_buffer = &g_uart_rx_buffer;
    uint8_t rx_char;

    flags = USART_IntGet(USART0);
    USART_IntClear(USART0, flags);

    /* Store incoming data into rx_buffer, set rx_data_ready when a full
     * line has been received
     */
    rx_char = USART_Rx(USART0);

    if (p_rx_uart_buffer->rx_count >= RXBUFFSIZE) // avoid overrun
    {
        return;
    }
    p_rx_uart_buffer->rx_buf[p_rx_uart_buffer->rx_tail_point] = rx_char;
    p_rx_uart_buffer->rx_tail_point = (p_rx_uart_buffer->rx_tail_point+1)%RXBUFFSIZE;

    p_rx_uart_buffer->rx_count++;

    if (p_rx_uart_buffer->rx_count > RXBUFFSIZE) // this will never happen
    {
        p_rx_uart_buffer->rx_head_point = p_rx_uart_buffer->rx_tail_point;
    }
    gecko_external_signal(EX_RX_HOST_MCU_UART);
}


//--------------------------------------------------------------------------------
// NAME      : USART0__TX_IRQHandler
// ABSTRACT  : Interrupt handler for transmitting on UART
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void USART0__TX_IRQHandler(void)
{
    uint32_t flags;
    flags = USART_IntGet(USART0);
    //USART_IntClear(USART0, flags);
    UART_TX_BUFFER_T *p_tx_uart_buffer = &g_uart_tx_buffer;

    if (flags & USART_IF_TXC)
    {
        if (uart_read_tx_buf(p_tx_uart_buffer) == 0)
        {
            /* clear USART interrupt */
            USART_IntClear(USART0, flags);
        }
    }
}
#endif
