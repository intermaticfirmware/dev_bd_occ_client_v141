//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: debug.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __DEBUG_TASK_H__
#define __DEBUG_TASK_H__

#include <stdio.h>
#include "uart.h"


#define DBG_TXBUFFSIZE 2000
#define DBG_RXBUFFSIZE 2000

typedef struct {
	uint8_t tx_buf[DBG_TXBUFFSIZE];
	int32_t tx_head_point;
	int32_t tx_tail_point;
	int32_t tx_count;
}UART_DBG_TX_BUFFER_T;

typedef struct {
	uint8_t rx_buf[DBG_RXBUFFSIZE];
	int32_t rx_head_point;
	int32_t rx_tail_point;
	int32_t rx_count;
}UART_DBG_RX_BUFFER_T;

void debug_vsprintf(const char *fmt, ...);

void debug_init(UART_DBG_TX_BUFFER_T *p_tx_uart_buffer, UART_DBG_RX_BUFFER_T *p_rx_uart_buffer);

extern UART_DBG_TX_BUFFER_T g_uart_debug_tx_buffer;
extern UART_DBG_RX_BUFFER_T g_uart_debug_rx_buffer;

#define DBGS 1 // remove or include from build all debug trace statements

               // provisioning logging
#define LOGI 1
#define LOGD 1
#define LOGE 1
#define LOGW 1

#define HOST 1 // SCM - Host communications

#define DB   1 // Database

#define LGHT 0
#define DTST 1 // for test code
#define DBGC 0
#define DEVT 0
#define LGTS 0

#define UINT8_ARRAY_DUMP(array_base, array_size)							\
	do {                                                                    \
        for (int i_log_exlusive = 0; i_log_exlusive<(array_size); i_log_exlusive++)                                \
            DEBUGV(LOGI, "%02X ", ((char*)(array_base))[i_log_exlusive]);							\
            DEBUGV(LOGI, "\r\n"); \
	} while(0)

// warning truncates string when its greater than buffer size
#if DBGS
#define DEBUGV(a, ...) {\
if (a) {\
    uint8_t uart_buff[80];\
    int32_t send_count;\
	send_count = snprintf((char*)uart_buff, sizeof(uart_buff), __VA_ARGS__);\
    debug_vsprintf((char*)uart_buff, send_count);\
    }\
}
#else
#define DEBUGV(...)
#endif

/* Imported variables ---------------------------------------------------------*/
/* Imported function prototypes -----------------------------------------------*/


#endif // __DEBUG_TASK_H__


