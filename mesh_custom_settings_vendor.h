//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: Contains constants, macros, structure
//  definitions, and public prototypes owned by the custom settings model
//
//-----------------------------------------------------------------------------
#ifndef _MESH_SETTINGS_VENDOR_H_
#define _MESH_SETTINGS_VENDOR_H_

#include "mesh_model_common.h"

#if (USE_NCP)
#include "ncp_gecko.h"
#else
#include "native_gecko.h"
#endif

// #defines and typedefs
#define SETTINGS_VENDOR_ELEMENT     0
#define SETTINGS_VENDOR_ID          0x02FF  // silicon labs vendor ID
#define SETTINGS_VENDOR_MODEL_ID    0x0001

typedef enum
{
    SETTINGS_WINK,
    SETTINGS_GET_SENSOR_MODEL,
    SETTINGS_GET_POWER_CFG,
    SETTINGS_GET_HOST_FW_VERSION,
    SETTINGS_GET_SCM_FW_VERSION_GET,
    SETTINGS_SET_ONOFF_SWITCH_CFG,
    SETTINGS_SET_ONOFF_SWITCH_CFG_UNACK = SETTINGS_SET_ONOFF_SWITCH_CFG,
    SETTINGS_SET_WIRED_LIGHT_OUTPUT,
    SETTINGS_GET_LOWBATTERY,
    SETTINGS_GET_HOST_ERROR,

    SETTING_MAX_PROPERTY = 0xffff
} MODULE_PROPERTY_ID_E;

// pack this struct to take up the least space in nvm
PACKSTRUCT(typedef struct
{
    uint32_t host_fw_version;
    uint8_t  power_type;
    uint8_t  host_model_num;
    uint8_t  onoff_switch_cfg;
    uint8_t  wired_light_output;
    uint8_t  host_lowbatt;
    uint8_t  host_error;
    uint8_t  self_provisioning_network;
    uint8_t  unused[21];
}) SETTINGS_NVM_T;

typedef struct
{
    MODULE_PROPERTY_ID_E property_id;
    uint8_t is_a_response;
} SETTINGS_MESSAGE_HEADER_T;

// pack this to overlay on top of a message buffer
PACKSTRUCT(typedef struct
{
    SETTINGS_MESSAGE_HEADER_T header;

    union
    {
        uint8_t string[64];
        uint8_t keep_for_packetsize[64];
        uint8_t wink_time_secs;
        uint8_t power_type;
        uint8_t host_model_num;
        uint8_t onoff_switch_cfg;
        uint8_t wired_light_output;
        uint8_t host_lowbatt;
        uint8_t host_error;
    };
}) SETTINGS_MESSAGE_DATA_T;

// Function prototypes
extern errorcode_t settings_model_init(void);
extern uint8_t gecko_evt_mesh_settings_vendor_model_receive_id_action(struct gecko_cmd_packet *evt);
extern errorcode_t settings_publish(SHARED_VENDOR_OP_CODES_E opcode, uint8_t length, uint8_t * data);

#endif // _MESH_SETTINGS_VENDOR_H_
