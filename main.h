//-----------------------------------------------------------------------------
//  All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: main.h contains constants, macros, structure definitions,
//               and public prototypes owned by main.
//
//-----------------------------------------------------------------------------
#ifndef __MAIN_H__
#define __MAIN_H__

// Includes
//******************************************************************************
#include "bg_types.h"


// Public Definitions and Constants
//******************************************************************************
#define RELEASE_BUILD		0
#define FAMILY_NAME			"Arista"
#define FW_VERSION          "01.00.002"

// This is a list of the various device types.  If a new device type is added,
// then simply append it to this list.
#define DEVICE_TYPE_ALC1_R       0
#define DEVICE_TYPE_ALC2_R       1
#define DEVICE_TYPE_ALC4_R       2
#define DEVICE_TYPE_OCC_SERVER   3
#define DEVICE_TYPE_HMI          4
#define DEVICE_TYPE_DLH_SERVER   5
// Also, make sure to set DEVICE_TYPE to the appropriate target.
#define DEVICE_TYPE             DEVICE_TYPE_OCC_SERVER
#define NUMBER_OF_ELEMENTS      1


#if (RELEASE_BUILD)
    #define SELF_PROVISION_ENABLED 0
    #define ENABLED_CLI 1            // TODO change this to 0 when we dont use CLI anymore
    #define DEBUG_ENABLED 1          // TODO change this to 0 when we dont use CLI anymore
#else
    #define USE_TEST_SETUPS 0
    #define SELF_PROVISION_ENABLED 1
    #define ENABLED_CLI 1
    #define DEBUG_ENABLED 1
#endif


// Macros - I/O definitions
//******************************************************************************

// Typedefs - Public structs, enums, etc.
//******************************************************************************

// Global Variable declarations (extern)
//******************************************************************************

// Prototype Definitions (extern)
//******************************************************************************

#endif  // __MAIN_H__
