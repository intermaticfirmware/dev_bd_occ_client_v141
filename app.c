//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: app.c application handler handles events from Bluetooth Stack.
//
//  if SELF_PROVISION_ENABLED is defined then device will provision itself
//  using a fixed key
//-----------------------------------------------------------------------------
#include "app.h"
#include "stdbool.h"
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "em_gpio.h"
#include "em_rtcc.h"
#include "em_cmu.h"

#include "vendor_sensor_model_def.h"
#include "mesh_generic_model_capi_types.h"
#include "mesh_custom_settings_vendor.h"
#include "mesh_model_common.h"
#include "debug.h"
#include "uart.h"
#include "wireless.h"
#include "timer_events.h"
#include "hal-config-devbd.h"

extern uint8_t boot_to_dfu;


/***********************************************************************************************//**
 * @addtogroup Application
 * @{
 **************************************************************************************************/
#define RES_100_MILLI_TICKS         3277
#define RES_1_SEC_TICKS				(32768)
#define RES_10_SEC_TICKS			((32768)*(10))
#define RES_10_MIN_TICKS			((32768)*(60)*(10))

#define RES_100_MILLI				0
#define RES_1_SEC					((1) << 6)
#define RES_10_SEC					((2) << 6)
#define RES_10_MIN					((3) << 6)
#define RES_BIT_MASK				0xC0

#define SIG_MODEL_ID            0xFFFF
#define GENERIC_ON_OFF_GRP_ADR  0xC001

// Max x is 63
#define SET_100_MILLI(x)                    (uint8_t)(RES_100_MILLI | ((x) & (0x3F)))
#define SET_1_SEC(x)                        (uint8_t)(RES_1_SEC | ((x) & (0x3F)))
#define SET_10_SEC(x)                       (uint8_t)(RES_10_SEC | ((x) & (0x3F)))
#define SET_10_MIN(x)                       (uint8_t)(RES_10_MIN | ((x) & (0x3F)))

uint32_t real_time_ticks = 0;
uint8_t update_interval = 0;
uint8_t conn_handle = 0xFF;
static uint8_t pb0_state = 0;
static uint8_t pb1_state = 0;
static uint32_t pb1_counter = 0;
static uint32_t pb0_counter = 0;
static uint8_t  setvar = 0;
static uint8_t  getvar = 0;

// desired settings sent to server
sensor_settings_t pir_settings = {
		.sensor_property_id = MOTION_SENSED_PROPERTY_ID,
		.sensor_setting_property_id = PIR_SENSITIVITY_ID,
		.sensor_setting[0] = PIR_SENSITIVITY_75,
        .sensor_setting[1] = 0,
        .sensor_setting[2] = 0,
        .sensor_setting[3] = 0
};

sensor_settings_t all_settings = {
        .sensor_property_id = MOTION_SENSED_PROPERTY_ID,
        .sensor_setting_property_id = 0xFF,
        .sensor_setting[0] = 0,
        .sensor_setting[1] = 0,
        .sensor_setting[2] = 0,
        .sensor_setting[3] = 0
};

sensor_settings_t hf_settings = {
		.sensor_property_id = MOTION_SENSED_PROPERTY_ID,
		.sensor_setting_property_id = HF_SENSITIVITY_ID,
		.sensor_setting[0] = HF_SENSITIVITY_75,
        .sensor_setting[1] = 0,
        .sensor_setting[2] = 0,
        .sensor_setting[3] = 0
};

sensor_settings_t trig_settings = {
        .sensor_property_id = MOTION_SENSED_PROPERTY_ID,
        .sensor_setting_property_id = TRIGGER_LOGIC_ID,
        .sensor_setting[0] = TRIGGER_LOGIC_PIR,
        .sensor_setting[1] = 0,
        .sensor_setting[2] = 0,
        .sensor_setting[3] = 0
};

sensor_settings_t occ_settings = {
		.sensor_property_id = MOTION_SENSED_PROPERTY_ID,
		.sensor_setting_property_id = OCC_MODE_ID,
		.sensor_setting[0] = OCCUPANCY_MODE,
        .sensor_setting[1] = 0,
        .sensor_setting[2] = 0,
        .sensor_setting[3] = 0
};

sensor_settings_t time_delay_settings = {
		.sensor_property_id = MOTION_SENSED_PROPERTY_ID,
		.sensor_setting_property_id = OCC_TIME_DELAY_ID,
		.sensor_setting[0] = 0x3C, // 60 sec
		.sensor_setting[1] = 0x00,
		.sensor_setting[2] = 0x00,
        .sensor_setting[3] = 0
};

sensor_settings_t walk_thru_settings = {
		.sensor_property_id = MOTION_SENSED_PROPERTY_ID,
		.sensor_setting_property_id = WALK_THRU_ID,
		.sensor_setting[0] = WALK_THRU_DIS,
        .sensor_setting[1] = 0,
        .sensor_setting[2] = 0,
        .sensor_setting[3] = 0
};

sensor_settings_t amblight_thresh = {
        .sensor_property_id = AMBIENT_LIGHT_PROPERTY_ID,
        .sensor_setting_property_id = AMB_LIGHT_COV_THRESH_ID,
        .sensor_setting[0] = 0xE8, // default = 1000
        .sensor_setting[1] = 0x03,
        .sensor_setting[2] = 0x00,
        .sensor_setting[3] = 0
};

sensor_settings_t amblight_scale = {
        .sensor_property_id = AMBIENT_LIGHT_PROPERTY_ID,
        .sensor_setting_property_id = AMB_LIGHT_SCALE_ID,
        .sensor_setting[0] = 0x00,
        .sensor_setting[1] = 0,
        .sensor_setting[2] = 0,
        .sensor_setting[3] = 0
};

sensor_settings_t amblight_reflux = {
        .sensor_property_id = AMBIENT_LIGHT_PROPERTY_ID,
        .sensor_setting_property_id = AMB_LIGHT_REF_LUX_ID,
        .sensor_setting[0] = 0x00,
        .sensor_setting[1] = 0x00,
        .sensor_setting[2] = 0x00,
        .sensor_setting[3] = 0
};

SETTINGS_MESSAGE_DATA_T msg_settings;

// ******************************
// settings received from server
// ******************************
sensor_msg_t rcvd_sensor_status;
sensor_settings_t rcvd_sensor_settings;

vendor_model_t sensor_occ_client = {
		.elem_index = PRIMARY_ELEMENT,
		.vendor_id = SILABS_VENDOR_ID,
		.model_id = VENDOR_OCC_SENSOR_CLIENT_ID,
		.publish = 1,
		.opcodes_len = NUMBER_OF_OPCODES,
		.opcodes_data[0] = sensor_get,
		.opcodes_data[1] = sensor_settings_set,
		.opcodes_data[2] = sensor_settings_get,
		.opcodes_data[3] = sensor_status,
		.opcodes_data[4] = sensor_setting_status
};

static void handle_gecko_event(uint32_t evt_id, struct gecko_cmd_packet *evt);
bool mesh_bgapi_listener(struct gecko_cmd_packet *evt);
//--------------------------------------------------------------------------------
// NAME      : key_interrupt_handler
// ABSTRACT  : interrupt handler for push buttons on dev board
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void key_interrupt_handler(void)
{
	uint32_t int_flag = GPIO_IntGet();
	GPIO_IntClear(int_flag);
#if DEV_BOARD
	if (int_flag & (1 << BSP_BUTTON0_PIN)) {
		pb0_state = !GPIO_PinInGet(BSP_BUTTON0_PORT, BSP_BUTTON0_PIN);
		if (pb0_state == 1) {
			// press, start counting
			pb0_counter = RTCC_CounterGet();
		} else {
			pb0_counter = RTCC_CounterGet() - pb0_counter;
			if (pb0_counter > 32768) {
				gecko_external_signal(EX_PB0_LONG_PRESS);
			} else {
				gecko_external_signal(EX_PB0_PRESS);
			}
		}
	}
	if (int_flag & (1 << BSP_BUTTON1_PIN)) {
		pb1_state = !GPIO_PinInGet(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN);
		if (pb1_state == 1) {
			// press, start counting
			pb1_counter = RTCC_CounterGet();
		} else {
			pb1_counter = RTCC_CounterGet() - pb1_counter;
			if (pb1_counter > 32768) {
				gecko_external_signal(EX_PB1_LONG_PRESS);
			} else {
				gecko_external_signal(EX_PB1_PRESS);
			}
	}
}
#else // actual board
	if (int_flag & (1 << RESET_BUTTON_PIN)) {
		gecko_external_signal(EX_RESET_PRESS);
		}
#endif		
}


//--------------------------------------------------------------------------------
// NAME      : GPIO_EVEN_IRQHandler
// ABSTRACT  : interrupt handler for push buttons on dev board
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void GPIO_EVEN_IRQHandler(void)
{
    key_interrupt_handler();
}


//--------------------------------------------------------------------------------
// NAME      : GPIO_ODD_IRQHandler
// ABSTRACT  : interrupt handler for push buttons on dev board
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void GPIO_ODD_IRQHandler(void)
{
    key_interrupt_handler();
}


//--------------------------------------------------------------------------------
// NAME      : initiate_factory_reset
// ABSTRACT  : erase ps flash then start a soft timer when timer expires performs
// a reset
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void initiate_factory_reset(void)
{
	/* if connection is open then close it before rebooting */
	if (conn_handle != 0xFF) {
		gecko_cmd_le_connection_close(conn_handle);
	}

	/* perform a factory reset by erasing PS storage. This removes all the keys and other settings
	 that have been configured for this node */
	gecko_cmd_flash_ps_erase_all();
	// reboot after a small delay
	gecko_cmd_hardware_set_soft_timer(2 * 32768, TIMER_ID_FACTORY_RESET, 1);
}


//--------------------------------------------------------------------------------
// NAME      : AppHandler
// ABSTRACT  :
// - waits until an event is received (blocking wait function)
// - returns a ptr to an event and gets passed to handle_gecko_event
// ARGUMENTS :
//   none
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void AppHandler(void)
{
#if DEV_BOARD // dev board
	GPIO_PinModeSet(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN, gpioModeInputPull, 1);
	GPIO_PinModeSet(BSP_BUTTON0_PORT, BSP_BUTTON0_PIN, gpioModeInputPull, 1);

	NVIC_EnableIRQ(GPIO_EVEN_IRQn);
	NVIC_EnableIRQ(GPIO_ODD_IRQn);

	GPIO_IntConfig(BSP_BUTTON0_PORT, BSP_BUTTON0_PIN, true, true, true);
	GPIO_IntConfig(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN, true, true, true);
#else // actual board
	GPIO_PinModeSet(RESET_BUTTON_PORT, RESET_BUTTON_PIN, gpioModeInputPull, 1);

	NVIC_EnableIRQ(GPIO_EVEN_IRQn);
	NVIC_EnableIRQ(GPIO_ODD_IRQn);

	GPIO_IntConfig(RESET_BUTTON_PORT, RESET_BUTTON_PIN, true, false, true);
#endif

	CMU_ClockEnable(cmuClock_RTCC, true);

	while (1) {
		struct gecko_cmd_packet *evt = gecko_wait_event();
		bool pass = mesh_bgapi_listener(evt);
		if (pass) {
			handle_gecko_event(BGLIB_MSG_ID(evt->header), evt);
		}
	}
}


//--------------------------------------------------------------------------------
// NAME      : handle_gecko_event
// ABSTRACT  : application and event handler
// ARGUMENTS : TODO
// RETURN    : None
//--------------------------------------------------------------------------------
static void handle_gecko_event(uint32_t evt_id, struct gecko_cmd_packet *evt)
{
	uint16_t temp_result;
	uint16_t sensor_type;
	uint8_t  i;
	static uint32_t prev_light_data = 0;
	uint32_t diff;

	switch (evt_id) {
	case gecko_evt_hardware_soft_timer_id:
		switch (evt->data.evt_hardware_soft_timer.handle)
		{
		    case TIMER_ID_FACTORY_RESET:
		    case TIMER_ID_RESET:
		        DEBUGV(LOGW,"Software reset.\r\n");
		        gecko_cmd_system_reset(0);
		        break;
		}
		break;

		case gecko_evt_le_connection_opened_id:
			DEBUGV(LOGI,"Connected.\r\n");
			conn_handle = evt->data.evt_le_connection_opened.connection;
			break;

		case gecko_evt_system_boot_id:
#if DEV_BOARD
			if (GPIO_PinInGet(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN) == 0) {
#else
			if (GPIO_PinInGet(RESET_BUTTON_PORT, RESET_BUTTON_PIN) == 0) {
#endif
				DEBUGV(LOGW,"Factory Reset Soon.\r\n");
				initiate_factory_reset();
			} else {
				DEBUGV(LOGI,"Normal start.\r\n");
				// Initialize Mesh stack in Node operation mode, wait for initialized event
				gecko_cmd_mesh_node_init();
			}
			break;

		case gecko_evt_mesh_vendor_model_receive_id: {
			uint16 setting_property_id;
			struct gecko_msg_mesh_vendor_model_receive_evt_t *recv_evt = (struct gecko_msg_mesh_vendor_model_receive_evt_t *) &evt->data;

            if (recv_evt->model_id == SETTINGS_VENDOR_MODEL_ID)
            {
                (void)gecko_evt_mesh_settings_vendor_model_receive_id_action(evt);
            }
            else if (recv_evt->model_id == VENDOR_OCC_SENSOR_CLIENT_ID)
            {
                DEBUGV(LOGD,"Vendor model data received.\r\n");
                DEBUGV(LOGD, "    VendorID = 0x%04X\r\n", recv_evt->vendor_id);
                DEBUGV(LOGD, "    ModelID = 0x%04X\r\n", recv_evt->model_id);
                DEBUGV(LOGD, "    Opcode = 0x%04X\r\n", recv_evt->opcode);

                switch (recv_evt->opcode)
                {
                    case sensor_status:
                        memcpy(&rcvd_sensor_status, &recv_evt->payload.data[0], recv_evt->payload.len);
                        DEBUGV(LOGI, "    PropID = 0x%04X\r\n", rcvd_sensor_status.header.property_id);
                        DEBUGV(LOGI, "    Payload Raw = Len:%d Data:", recv_evt->payload.len);
                        for (i = 0; i < recv_evt->payload.len; i++)
                        {
                            DEBUGV(LOGI, "0x%02X ", recv_evt->payload.data[i])
                        }
                        DEBUGV(LOGI, "\r\n");

                        if (rcvd_sensor_status.header.property_id == MOTION_SENSED_PROPERTY_ID)
                        {
                            DEBUGV(LOGI,"    Payload: Occ sensor status = %d\r\n", rcvd_sensor_status.occ_data);
                        }
                        else if (rcvd_sensor_status.header.property_id == AMBIENT_LIGHT_PROPERTY_ID)
                        {
                            if (rcvd_sensor_status.light_data > prev_light_data)
                            {
                                diff = rcvd_sensor_status.light_data - prev_light_data;
                                DEBUGV(LOGI, "    Payload: Ambient Light = %lu [+%lu]\r\n", rcvd_sensor_status.light_data, diff);
                                prev_light_data = rcvd_sensor_status.light_data;
                            }
                            else
                            {
                                diff = prev_light_data - rcvd_sensor_status.light_data;
                                DEBUGV(LOGI, "    Payload: Ambient Light = %lu [-%lu]\r\n", rcvd_sensor_status.light_data, diff);
                                prev_light_data = rcvd_sensor_status.light_data;
                            }
                        }
                        break;

                    case sensor_setting_status:
                        setting_property_id = ((recv_evt->payload.data[3] << 8) | recv_evt->payload.data[2]);
                        sensor_type = ((recv_evt->payload.data[1] << 8) | recv_evt->payload.data[0]);
                        memcpy(&rcvd_sensor_settings, &recv_evt->payload.data[0], recv_evt->payload.len);
                        DEBUGV(LOGI, "    PropID = 0x%04X\r\n", sensor_type);
                        DEBUGV(LOGD, "    Payload Raw: ");
                        for (i = 0; i < recv_evt->payload.len; i++)
                        {
                            DEBUGV(LOGD, "0x%02X ", recv_evt->payload.data[i]);
                        }
                        DEBUGV(LOGD, "\r\n");

                        switch (setting_property_id)
                        {
                            case PIR_SENSITIVITY_ID:
                                if (sensor_type == MOTION_SENSED_PROPERTY_ID)
                                {
                                    DEBUGV(LOGD, "    Payload: PIR_SENSITIVITY  setting = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[0]);
                                }
                                break;

                            case HF_SENSITIVITY_ID:
                                if (sensor_type == MOTION_SENSED_PROPERTY_ID)
                                {
                                    DEBUGV(LOGD, "    Payload: HF_SENSITIVITY  setting = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[0]);
                                }
                                break;

                            case TRIGGER_LOGIC_ID:
                                if (sensor_type == MOTION_SENSED_PROPERTY_ID)
                                {
                                    DEBUGV(LOGD, "    Payload: TRIGGER_LOGIC  setting = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[0]);
                                }
                                break;

                            case OCC_MODE_ID:
                                if (sensor_type == MOTION_SENSED_PROPERTY_ID)
                                {
                                    DEBUGV(LOGD, "    Payload: OCC_MODE  setting = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[0]);
                                }
                                break;

                            case WALK_THRU_ID:
                                if (sensor_type == MOTION_SENSED_PROPERTY_ID)
                                {
                                    DEBUGV(LOGD, "    Payload: WALK_THRU  setting = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[0]);
                                }
                                break;

                            case OCC_TIME_DELAY_ID:
                                if (sensor_type == MOTION_SENSED_PROPERTY_ID)
                                {
                                    DEBUGV(LOGD, "    Payload: TIME_DELAY  setting = 0x%02X 0x%02X 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[0], rcvd_sensor_settings.sensor_setting[1], rcvd_sensor_settings.sensor_setting[2]);
                                }
                                break;

                            case AMB_LIGHT_COV_THRESH_ID:
                            case AMB_LIGHT_SCALE_ID:
                            case AMB_LIGHT_REF_LUX_ID:
                                DEBUGV(LOGD, "    DLHSensor Property - This is an unsupported PropertyID\r\n");
                                break;

                            case ALL_PROPERTIES_ID:
                                if (sensor_type == MOTION_SENSED_PROPERTY_ID)
                                {
                                    DEBUGV(LOGD, "    Payload: Host_Model_Num setting     = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[0]);
                                    DEBUGV(LOGD, "    Payload: Host_FW_Ver setting        = 0x%02X 0x%02X 0x%02X 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[1], rcvd_sensor_settings.sensor_setting[2], rcvd_sensor_settings.sensor_setting[3], rcvd_sensor_settings.sensor_setting[4]);
                                    DEBUGV(LOGD, "    Payload: SCM_FW_Ver setting         = 0x%02X 0x%02X 0x%02X 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[5], rcvd_sensor_settings.sensor_setting[6], rcvd_sensor_settings.sensor_setting[7], rcvd_sensor_settings.sensor_setting[8]);
                                    DEBUGV(LOGD, "    Payload: Host_Power_Type setting    = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[9]);
                                    DEBUGV(LOGD, "    Payload: OnOff_Switch_Cfg setting   = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[10]);
                                    DEBUGV(LOGD, "    Payload: Wired_Light_Output setting = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[11]);
                                    DEBUGV(LOGD, "    Payload: Host_LowBatt setting       = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[12]);
                                    DEBUGV(LOGD, "    Payload: Host_ErrCode setting       = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[13]);
                                    DEBUGV(LOGD, "    Payload: PIR_SENSITIVITY setting    = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[14]);
                                    DEBUGV(LOGD, "    Payload: HF_SENSITIVITY setting     = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[15]);
                                    DEBUGV(LOGD, "    Payload: TRIGGER_LOGIC setting      = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[16]);
                                    DEBUGV(LOGD, "    Payload: OCC_MODE setting           = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[17]);
                                    DEBUGV(LOGD, "    Payload: TIME_DELAY  setting        = 0x%02X 0x%02X 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[18], rcvd_sensor_settings.sensor_setting[19], rcvd_sensor_settings.sensor_setting[20]);
                                    DEBUGV(LOGD, "    Payload: WALK_THRU setting          = 0x%02X\r\n", rcvd_sensor_settings.sensor_setting[21]);
                                    DEBUGV(LOGD, "    Payload: AmbLight COV setting       = Do Not Care\r\n");
                                    DEBUGV(LOGD, "    Payload: AmbLight Scaled Range      = Do Not Care\r\n");
                                    DEBUGV(LOGD, "    Payload: AmbLight Ref Lux           = Do Not Care\r\n");
                                    DEBUGV(LOGD, "    Payload: Unused                     = Do Not Care\r\n");
                                }
                                break;

                            default:
                                DEBUGV(LOGD, "    Payload:  INVALID Property ID\r\n");
                                break;
                        }
                        break;

                    default:
                        // Client should not response to other opcodes
                        break;
                }
            }
            else
            {
                DEBUGV(LOGD, "Error:  Invalid Model ID\r\n");
            }
        }
        break;

		case gecko_evt_system_external_signal_id: {
		    uint8_t opcode = 0;
		    uint8_t length = 0;
		    struct gecko_msg_mesh_vendor_model_set_publication_rsp_t *set_pub_ret;
		    struct gecko_msg_mesh_vendor_model_publish_rsp_t *pub_ret;
		    uint8_t *pay;
		    uint8_t i;

#if 0
// Custom Settings Tests
		    SETTINGS_MESSAGE_DATA_T msg_settings;
		    data = data;        // to avoid compiler warning
            if (evt->data.evt_system_external_signal.extsignals & EX_PB0_LONG_PRESS)
            {
                DEBUGV(LOGW,"Factory Reset Now!!\r\n");
                initiate_factory_reset();
                return;
            }
            if (evt->data.evt_system_external_signal.extsignals & EX_PB0_PRESS)
            {
                DEBUGV(LOGD,"PB0 Pressed.- sensor_custom_settings_op_get sent\r\n");
                memset(&msg_settings, 0, sizeof(msg_settings));
                msg_settings.header.is_a_response = 0;
                opcode = SENSOR_CUSTOM_SETTINGS_OP_GET;

                switch (getvar)
                {
                    case 0:
                        DEBUGV(LOGD, "   Get Wink\r\n");
                        msg_settings.header.property_id = SETTINGS_WINK;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.wink_time_secs);
                        break;
                    case 1:
                        DEBUGV(LOGD, "   Get Host Sensor Model\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_SENSOR_MODEL;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.host_model_num);
                        break;
                    case 2:
                        DEBUGV(LOGD, "   Get Power Configuration\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_POWER_CFG;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.power_type);
                        break;
                    case 3:
                        DEBUGV(LOGD, "   Get Host FW Version\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_HOST_FW_VERSION;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.string);
                        break;
                    case 4:
                        DEBUGV(LOGD, "   Get SCM FW Version\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_SCM_FW_VERSION_GET;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.string);
                        break;
                    case 5:
                        DEBUGV(LOGD, "   Get OnOff Switch Cfg\r\n");
                        msg_settings.header.property_id = SETTINGS_SET_ONOFF_SWITCH_CFG;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.onoff_switch_cfg);
                        break;
                    case 6:
                        DEBUGV(LOGD, "   Get Wired Light Output\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_LOWBATTERY;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.host_lowbatt);
                        break;
                    case 7:
                        DEBUGV(LOGD, "   Get LowBatt Error Code\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_LOWBATTERY;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.host_lowbatt);
                        break;
                    case 8:
                        DEBUGV(LOGD, "   Get Host Error Code\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_HOST_ERROR;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.host_error);
                        break;
                    default:
                        DEBUGV(LOGD, "   Get an invalid property id on purpose\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_HOST_ERROR + 1;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + 1;
                        getvar = 0;
                }
                (void)settings_publish(opcode, length, (uint8_t*)&msg_settings);
                getvar++;
            }
            if (evt->data.evt_system_external_signal.extsignals & EX_PB1_PRESS)
            {
                DEBUGV(LOGD,"PB1 Pressed.- sensor_custom_settings_op_set sent\r\n");
                memset(&msg_settings, 0, sizeof(msg_settings));
                msg_settings.header.is_a_response = 0;
                opcode = SENSOR_CUSTOM_SETTINGS_OP_SET;

                switch (setvar)
                {
                    case 0:
                        DEBUGV(LOGD, "   Set OnOff Switch Cfg\r\n");
                        msg_settings.header.property_id = SETTINGS_SET_ONOFF_SWITCH_CFG;
                        msg_settings.onoff_switch_cfg = 0;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.onoff_switch_cfg);
                        break;
                    case 1:
                        DEBUGV(LOGD, "   Wink\r\n");
                        msg_settings.header.property_id = SETTINGS_WINK;
                        msg_settings.wink_time_secs = 24;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.wink_time_secs);
                        break;
                    case 2:
                        DEBUGV(LOGD, "   Wired Light Output\r\n");
                        msg_settings.header.property_id = SETTINGS_SET_WIRED_LIGHT_OUTPUT;
                        msg_settings.wired_light_output = 1;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + sizeof(msg_settings.wired_light_output);
                        break;
                    default:
                        DEBUGV(LOGD, "   Set an invalid property id on purpose\r\n");
                        msg_settings.header.property_id = SETTINGS_GET_POWER_CFG;
                        length = sizeof(SETTINGS_MESSAGE_HEADER_T) + 1;
                        setvar = 0;
                }
                (void)settings_publish(opcode, length, (uint8_t*)&msg_settings);
                setvar++;
            }
	        if (evt->data.evt_system_external_signal.extsignals & EX_PB1_LONG_PRESS)
	        {
	            DEBUGV(LOGD,"PB1 Long Pressed.- Not presently used\r\n");
	        }
#else
            if (evt->data.evt_system_external_signal.extsignals & EX_PB0_PRESS)
            {
                opcode = sensor_get;
                pir_settings.sensor_setting[0] = 0;
                pir_settings.sensor_setting_property_id = 0;
                length = sizeof(sensor_settings_t);
                set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8_t*)&pir_settings);
                DEBUGV(LOGD,"PB0 Pressed.- sensor_get msg sent\r\n");
            }
            if (evt->data.evt_system_external_signal.extsignals & EX_PB0_LONG_PRESS)
            {
                DEBUGV(LOGW,"Factory Reset Now!!\r\n");
                initiate_factory_reset();
                return;
            }
            if (evt->data.evt_system_external_signal.extsignals & EX_PB1_LONG_PRESS)
            {
	            opcode = sensor_settings_set;
	            DEBUGV(LOGD,"PB1 Long Pressed. - sensor_settings_set msg sent\r\n");
                DEBUGV(LOGD, " Prepare the publication data ....\r\n");
                DEBUGV(LOGD, "   VendorID: 0x%04X\r\n", sensor_occ_client.vendor_id);
                DEBUGV(LOGD, "   ModelID:  0x%04X\r\n", sensor_occ_client.model_id);
                DEBUGV(LOGD, "   OpCode:   0x%04X\r\n", opcode);

                switch (setvar)
                {
#if (DEVICE_TYPE == DEVICE_TYPE_OCC_SERVER)
                    case 0:
                        DEBUGV(LOGD, "   Set pir_settings = 3, Payload = ");
                        pir_settings.sensor_setting[0] = PIR_SENSITIVITY_100;
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&pir_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&pir_settings);
                        break;

                    case 1:
                        DEBUGV(LOGD, "   Set hf_settings = 2, Payload = ");
                        hf_settings.sensor_setting[0] = HF_SENSITIVITY_75;
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&hf_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&hf_settings);
                        break;

                    case 2:
                        DEBUGV(LOGD, "   Set trig_settings = 3, Payload = ");
                        trig_settings.sensor_setting[0] = TRIGGER_LOGIC_PIR_HF;
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&trig_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&trig_settings);
                        break;

                    case 3:
                        DEBUGV(LOGD, "   Set occ_settings = 1, Payload = ");
                        occ_settings.sensor_setting[0] = VACANCY_MODE;
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&occ_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&occ_settings);
                        break;

                    case 4:
                        DEBUGV(LOGD, "   Set time_delay_settings = 516, Payload = ");
                        time_delay_settings.sensor_setting[0] = 0x04;
                        time_delay_settings.sensor_setting[1] = 0x02;
                        time_delay_settings.sensor_setting[2] = 0x00;
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&time_delay_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&time_delay_settings);
                        break;

                    case 5:
                        DEBUGV(LOGD, "   Set walk_thru_settings = 1, Payload = ");
                        walk_thru_settings.sensor_setting[0] = WALK_THRU_EN;
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&walk_thru_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&walk_thru_settings);
                        break;
#endif

                    default:
                        DEBUGV(LOGD, "INVALID PROPERTY ID\r\n");
                        setvar = 0;
                        break;
                }
                if (++setvar > 5)
                {
                    setvar = 0;
                }
	        }
	        if (evt->data.evt_system_external_signal.extsignals & EX_PB1_PRESS)
	        {
	            opcode = sensor_settings_get;
	            DEBUGV(LOGD,"\r\nPB1 Pressed. - sensor_settings_get msg will be sent\r\n");
                DEBUGV(LOGD, " Prepare the publication data ....\r\n");
                DEBUGV(LOGD, "   VendorID: 0x%04X\r\n", sensor_occ_client.vendor_id);
                DEBUGV(LOGD, "   ModelID:  0x%04X\r\n", sensor_occ_client.model_id);
                DEBUGV(LOGD, "   OpCode:   0x%04X\r\n", opcode);

                switch (getvar)
                {
#if (DEVICE_TYPE == DEVICE_TYPE_OCC_SERVER)
                    case 0:
                        DEBUGV(LOGD, "   Get pir_settings Payload = ");
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&pir_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&pir_settings);
                        break;

                    case 1:
                        DEBUGV(LOGD, "   Get hf_settings Payload = ");
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&hf_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&hf_settings);
                        break;

                    case 2:
                        DEBUGV(LOGD, "   Get trigger Payload = ");
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&trig_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&trig_settings);
                        break;

                    case 3:
                        DEBUGV(LOGD, "   Get mode Payload = ");
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&occ_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&occ_settings);
                        break;

                    case 4:
                        DEBUGV(LOGD, "   Get time_delay_settings Payload = ");
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&time_delay_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&time_delay_settings);
                        break;

                    case 5:
                        DEBUGV(LOGD, "   Get walk_thru_settings Payload = ");
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&walk_thru_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&walk_thru_settings);
                        break;

                    case 6:
                        DEBUGV(LOGD, "   Get ALL Settings = ");
                        length = sizeof(sensor_settings_t);
                        pay = (uint8_t *)&all_settings;
                        for (i = 0; i < length; i++)
                        {
                            DEBUGV(LOGD, " %02X ", *pay);
                            pay++;
                        }
                        DEBUGV(LOGD, "\r\n\r\n");
                        set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, opcode, 1, length, (uint8*)&all_settings);
                        break;
#endif
                    default:
                        DEBUGV(LOGD, "INVALID PROPERTY ID\r\n");
                        getvar = 0;
                        break;
                }

                if (++getvar > 6)
                {
                    getvar = 0;
                }
	        }

            if (set_pub_ret->result)
            {
                DEBUGV(LOGE,"Set publication error = 0x%04X\r\n", set_pub_ret->result);
            }
            else
            {
	            DEBUGV(LOGD,"Set publication done. Publishing...\r\n");
	            pub_ret = gecko_cmd_mesh_vendor_model_publish(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id);
	            if (pub_ret->result) {
	                DEBUGV(LOGE,"Publish error = 0x%04X\r\n", pub_ret->result);
	            }
	            else
	            {
	                DEBUGV(LOGD,"Publish done.\r\n\r\n");
	            }
	        }
#endif
	    }
	    break;

		case gecko_evt_mesh_node_initialized_id: {
		    struct gecko_msg_mesh_node_initialized_evt_t *node_init_ret = (struct gecko_msg_mesh_node_initialized_evt_t *) &evt->data;
		    struct gecko_msg_mesh_vendor_model_init_rsp_t *vm_init_ret;
#if (SELF_PROVISION_ENABLED)
		    static aes_key_128 enc_key = {.data = "\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A\x0A"};
#endif
		    vm_init_ret = gecko_cmd_mesh_vendor_model_init(sensor_occ_client.elem_index, sensor_occ_client.vendor_id, sensor_occ_client.model_id, sensor_occ_client.publish, sensor_occ_client.opcodes_len, sensor_occ_client.opcodes_data);
		    if (vm_init_ret->result) {
		        DEBUGV(LOGE,"Vendor model init error = 0x%04X\r\n", vm_init_ret->result);
		    } else {
		        DEBUGV(LOGI,"Vendor model init done. --- ");
		        DEBUGV(LOGW,"Client. \r\n");
		    }

		    settings_model_init();      // Initialize Custom Settings model

		    if (node_init_ret->provisioned) {
		        // provisioned already
		        DEBUGV(LOGI,"Provisioned already.\r\n");
#if (SELF_PROVISION_ENABLED)
		        struct gecko_msg_mesh_test_get_local_model_sub_rsp_t *sub_setting = gecko_cmd_mesh_test_get_local_model_sub(PRIMARY_ELEMENT, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID);
		        uint16_t sub_address = sub_setting->addresses.data[0] | (sub_setting->addresses.data[1] << 8);
		        if (!sub_setting->result && (sub_address == GENERIC_ON_OFF_GRP_ADR))
		        {
		            DEBUGV(LOGI,"Configuration done already.\n\r");
		        }
                else
                {
                    // Setup models. This will only happen after a node is first provisioned
                    temp_result = gecko_cmd_mesh_test_add_local_key(1, enc_key, 0, 0)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_add_local_key 0x%x %s\n\r", MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, get_api_error_text(temp_result));

                    // Bind the Generic On/Off server
                    temp_result = gecko_cmd_mesh_test_bind_local_model_app(PRIMARY_ELEMENT, 0, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_bind_local_model_app 0x%x %s\n\r", MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, get_api_error_text(temp_result));

                    // Bind the Sensor Vendor client
                    temp_result = gecko_cmd_mesh_test_bind_local_model_app(PRIMARY_ELEMENT, 0, SILABS_VENDOR_ID, VENDOR_OCC_SENSOR_CLIENT_ID)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_bind_local_model_app 0x%x %s\n\r", VENDOR_OCC_SENSOR_CLIENT_ID, get_api_error_text(temp_result));

                    // Bind the Sensor Custom Settings client
                    temp_result = gecko_cmd_mesh_test_bind_local_model_app(PRIMARY_ELEMENT, 0, SETTINGS_VENDOR_ID, SETTINGS_VENDOR_MODEL_ID)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_bind_local_model_app 0x%x %s\n\r", SETTINGS_VENDOR_MODEL_ID, get_api_error_text(temp_result));

                    // additional setup normally done by provisioner
                    temp_result = gecko_cmd_mesh_test_set_relay(1, 0, 0)->result;	// turn relay feature on
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_set_relay %s\n\r", get_api_error_text(temp_result));

                    temp_result = gecko_cmd_mesh_test_set_nettx(2, 4)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_set_nettx %s\n\r", get_api_error_text(temp_result));

                    // Subscribe to Generic On/Off server
                    temp_result = gecko_cmd_mesh_test_add_local_model_sub(PRIMARY_ELEMENT, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, 0xC001)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_add_local_model_sub 0x%x %s\n\r", MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, get_api_error_text(temp_result));

                    // Subscribe to Sensor Vendor client
                    temp_result = gecko_cmd_mesh_test_add_local_model_sub(PRIMARY_ELEMENT, SILABS_VENDOR_ID, VENDOR_OCC_SENSOR_CLIENT_ID, 0xC001)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_add_local_model_sub 0x%x %s\n\r", VENDOR_OCC_SENSOR_CLIENT_ID, get_api_error_text(temp_result));

                    // Subscribe to Custom Settings client
                    temp_result = gecko_cmd_mesh_test_add_local_model_sub(PRIMARY_ELEMENT, SETTINGS_VENDOR_ID, SETTINGS_VENDOR_MODEL_ID, 0xC001)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_add_local_model_sub 0x%x %s\n\r", SETTINGS_VENDOR_MODEL_ID, get_api_error_text(temp_result));

                    // Publish to Generic On/Off Server
                    temp_result = gecko_cmd_mesh_test_set_local_model_pub(PRIMARY_ELEMENT, 0, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, 0xC001, 5, 0, 0, 0)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_set_local_model_pub 0x%x %s\n\r", MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, get_api_error_text(temp_result));

                    // Publish to Sensor Vendor Client
                    temp_result = gecko_cmd_mesh_test_set_local_model_pub(PRIMARY_ELEMENT, 0, SILABS_VENDOR_ID, VENDOR_OCC_SENSOR_CLIENT_ID, 0xC001, 5, 0, 0, 0)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_add_local_model_sub 0x%x %s\n\r", VENDOR_OCC_SENSOR_CLIENT_ID, get_api_error_text(temp_result));

                    // Publish to Custom Settings Client
                    temp_result = gecko_cmd_mesh_test_set_local_model_pub(PRIMARY_ELEMENT, 0, SETTINGS_VENDOR_ID, SETTINGS_VENDOR_MODEL_ID, 0xC001, 5, 0, 0, 0)->result;
                    DEBUGV(LOGI,"gecko_cmd_mesh_test_add_local_model_sub 0x%x %s\n\r", SETTINGS_VENDOR_MODEL_ID, get_api_error_text(temp_result));
                }
#endif
            }
		    else
		    {
		        DEBUGV(LOGI,"node is unprovisioned\r\n");
#if (SELF_PROVISION_ENABLED)
		        DEBUGV(LOGI,"Self Provisioning...\r\n");
		        struct gecko_msg_system_get_bt_address_rsp_t *bd_ret = gecko_cmd_system_get_bt_address();
		        temp_result = gecko_cmd_mesh_node_set_provisioning_data(enc_key, enc_key, 0, 0, ((bd_ret->address.addr[1] << 8) | bd_ret->address.addr[0]) & 0x7FFF, 0)->result;

		        DEBUGV(LOGI,"gecko_cmd_mesh_node_set_provisioning_data %s\n\r", get_api_error_text(temp_result));
		        gecko_cmd_hardware_set_soft_timer(32768, TIMER_ID_RESET, 1);
#else
		        // The Node is now initialized, start unprovisioned Beaconing using PB-Adv Bearer
		        gecko_cmd_mesh_node_start_unprov_beaconing(0x3);
		        DEBUGV(LOGI,"Unprovisioned beaconing.\r\n");
#endif
		    }
		}
		break;

	    case gecko_evt_mesh_node_provisioned_id:
	        DEBUGV(LOGI,"Provisioning done.\r\n");
	        break;

        case gecko_evt_mesh_node_provisioning_failed_id:
            DEBUGV(LOGI,"Provisioning failed. Result = 0x%04x\r\n", evt->data.evt_mesh_node_provisioning_failed.result);
            break;

        case gecko_evt_mesh_node_provisioning_started_id:
            DEBUGV(LOGI,"Provisioning started.\r\n");
            break;

        case gecko_evt_mesh_node_key_added_id:
            DEBUGV(LOGI,"got new %s key with index %x\r\n", evt->data.evt_mesh_node_key_added.type == 0 ? "network" : "application", evt->data.evt_mesh_node_key_added.index);
            break;

        case gecko_evt_mesh_node_config_set_id: {
            DEBUGV(LOGD,"gecko_evt_mesh_node_config_set_id\r\n\t");
            struct gecko_msg_mesh_node_config_set_evt_t *set_evt = (struct gecko_msg_mesh_node_config_set_evt_t *) &evt->data;
            UINT8_ARRAY_DUMP(set_evt->value.data, set_evt->value.len);
        }
        break;

        case gecko_evt_mesh_node_model_config_changed_id:
            DEBUGV(LOGI,"model config changed\r\n");
            break;

        case gecko_evt_le_connection_closed_id:
            DEBUGV(LOGI,"Disconnected.\r\n");
            conn_handle = 0xFF;
            /* Check if need to boot to dfu mode */
            if (boot_to_dfu) {
                /* Enter to DFU OTA mode */
                gecko_cmd_system_reset(2);
            }
            break;

        case gecko_evt_gatt_server_user_write_request_id:
            if (evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
                /* Set flag to enter to OTA mode */
                boot_to_dfu = 1;
                /* Send response to Write Request */
                gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection,
                    gattdb_ota_control, bg_err_success);

                /* Close connection to enter to DFU OTA mode */
                gecko_cmd_le_connection_close(evt->data.evt_gatt_server_user_write_request.connection);
            }
            break;

        default:
            break;
	}
}
