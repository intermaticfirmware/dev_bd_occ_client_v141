//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: debug.c is responsible for debug output and input
//
//-----------------------------------------------------------------------------
#include "em_usart.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "hal-config-devbd.h"
#include "debug.h"
#include <stdio.h>
#include <stdarg.h>
#include "em_core.h"


static int debug_read_tx_buf (UART_DBG_TX_BUFFER_T *p_uart_buf);
int32_t debug_put_string(uint8_t *buffer, int32_t num_char);

UART_DBG_TX_BUFFER_T g_uart_debug_tx_buffer;
UART_DBG_RX_BUFFER_T g_uart_debug_rx_buffer;
//--------------------------------------------------------------------------------
// NAME      : debug_init
// ABSTRACT  : This function initializes the debug uart port and debug buffers.
// ARGUMENTS :
//   Buffer used for uart transmission
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void debug_init(UART_DBG_TX_BUFFER_T *p_tx_uart_buffer, UART_DBG_RX_BUFFER_T *p_rx_uart_buffer)
{
	USART_InitAsync_TypeDef init = USART_INITASYNC_DEFAULT;
	p_tx_uart_buffer->tx_head_point = 0;
	p_tx_uart_buffer->tx_tail_point = 0;
	p_tx_uart_buffer->tx_count = 0;

	p_rx_uart_buffer->rx_head_point = 0;
	p_rx_uart_buffer->rx_tail_point = 0;
	p_rx_uart_buffer->rx_count = 0;

	// Enable oscillator to GPIO and USART1 modules
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_USART1, true);

	// configure UART
	GPIO_PinModeSet( DBG_PORT, DBG_TX_PIN, gpioModePushPull, 1 );
	GPIO_PinModeSet( DBG_PORT, DBG_RX_PIN, gpioModeInputPull, 0 );

	// Initialize USART asynchronous mode and route pins
	USART_InitAsync(USART1, &init);

	USART1->ROUTEPEN |= USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
	USART1->ROUTELOC0 = DBG_RXLOC | DBG_TXLOC;

	//Initialize USART Interrupts
	USART_IntEnable(USART1, USART_IEN_RXDATAV);
	USART_IntEnable(USART1, USART_IEN_TXC);

	//Enabling USART Interrupts
	NVIC_EnableIRQ(USART1_RX_IRQn);
	NVIC_EnableIRQ(USART1_TX_IRQn);
}
//--------------------------------------------------------------------------------
// NAME      : debug_vsprintf
// ABSTRACT  : This function works like a printf function
// ARGUMENTS :
//   printf style
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void debug_vsprintf(const char *fmt, ...)
{
	int32_t buf_count;
	int32_t send_count;

	va_list va;
	char buffer[200];
	va_start(va, fmt);
	buf_count = vsprintf(buffer, fmt, va);
	if (buf_count > 0)
		send_count = debug_put_string((uint8_t *)buffer, buf_count);
	if (send_count != buf_count)
		buf_count = 0;
	va_end(va);
}
//--------------------------------------------------------------------------------
// NAME      : debug_put_string
// ABSTRACT  : This function puts a string into a buffer
// ARGUMENTS :
// RETURN    :
//   int     : returns number of characters added to buffer
//--------------------------------------------------------------------------------
int32_t debug_put_string(uint8_t *buffer, int32_t num_char)
{
	uint8_t *p_buffer = buffer;
	uint8_t ch;
	int32_t num_tx;

	UART_DBG_TX_BUFFER_T *p_tx_uart_buffer;
	int32_t send_count = 0;

	CORE_DECLARE_IRQ_STATE;

	p_tx_uart_buffer = &g_uart_debug_tx_buffer;

	for ( num_tx = 0; num_tx < num_char; num_tx++)
	{
		CORE_ENTER_ATOMIC();
		/* stop before over running the buffer */
		if (p_tx_uart_buffer->tx_count >= DBG_TXBUFFSIZE)
		{
			CORE_EXIT_ATOMIC();
			break;
		}
		ch = *p_buffer & 0xff;
		p_tx_uart_buffer->tx_buf[p_tx_uart_buffer->tx_tail_point] = ch;
		p_tx_uart_buffer->tx_tail_point = (p_tx_uart_buffer->tx_tail_point+1)%DBG_TXBUFFSIZE;
		/* start critical section */

		p_tx_uart_buffer->tx_count++;
		/* end of critical section */
		CORE_EXIT_ATOMIC();
		send_count++;
		p_buffer++;
	}
    /* enable USART interrupt */
	USART_IntSet(USART1, USART_IFS_TXC);
    return (send_count);

}
//--------------------------------------------------------------------------------
// NAME      : debug_read_tx_buf
// ABSTRACT  : This function reads a character from the buffer and puts it into
//             the transmit buffer
// ARGUMENTS :
// RETURN    :
//   int     : returns the number of characters in the buffer
//--------------------------------------------------------------------------------
static int debug_read_tx_buf (UART_DBG_TX_BUFFER_T *p_tx_uart_buffer)
{
	if (p_tx_uart_buffer->tx_count == 0)
		return 0;

	USART_Tx(USART1, p_tx_uart_buffer->tx_buf[p_tx_uart_buffer->tx_head_point]);
	p_tx_uart_buffer->tx_head_point = (p_tx_uart_buffer->tx_head_point+1)%DBG_TXBUFFSIZE;
	p_tx_uart_buffer->tx_count--;
	return(p_tx_uart_buffer->tx_count);
}
//--------------------------------------------------------------------------------
// NAME      : USART1_TX_IRQHandler
// ABSTRACT  : Interrupt handler for transmitting on UART
// ARGUMENTS :
//   none
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void USART1_TX_IRQHandler(void)
{
  uint32_t flags;
  flags = USART_IntGet(USART1);
  //USART_IntClear(USART1, flags);
  UART_DBG_TX_BUFFER_T *p_tx_uart_buffer = &g_uart_debug_tx_buffer;

  if (flags & USART_IF_TXC)
  {
    if (debug_read_tx_buf(p_tx_uart_buffer) == 0)
    {
    	/* clear USART interrupt */
    	USART_IntClear(USART1, flags);
    }
  }
}
//--------------------------------------------------------------------------------
// NAME      : debug_read_rx_buf
// ABSTRACT  : This function reads a character from the buffer
//
// ARGUMENTS :
// RETURN    :
//   int     : returns the number of characters in the buffer
//--------------------------------------------------------------------------------
uint8_t debug_read_rx_buf (UART_DBG_RX_BUFFER_T *p_rx_uart_buffer, uint8_t *len)
{
	uint8_t read_char;
	CORE_DECLARE_IRQ_STATE;

	/* start critical section */
	CORE_ENTER_ATOMIC();
	if (p_rx_uart_buffer->rx_count == 0)
	{
		*len = 0;
	/* end of critical section */
	CORE_EXIT_ATOMIC();
		return 0;
	}

	read_char = p_rx_uart_buffer->rx_buf[p_rx_uart_buffer->rx_head_point];
	p_rx_uart_buffer->rx_head_point = (p_rx_uart_buffer->rx_head_point+1)%DBG_RXBUFFSIZE;

	p_rx_uart_buffer->rx_count--;
	*len = 1;
	/* end of critical section */
	CORE_EXIT_ATOMIC();
	return(read_char);
}
//--------------------------------------------------------------------------------
// NAME      : USART1_RX_IRQHandler
// ABSTRACT  : Interrupt handler for transmitting on UART
// ARGUMENTS :
//   none
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void USART1_RX_IRQHandler(void)
{
  uint32_t flags;
  UART_DBG_RX_BUFFER_T *p_rx_uart_buffer = &g_uart_debug_rx_buffer;
  uint8_t rx_char;

  flags = USART_IntGet(USART1);
  USART_IntClear(USART1, flags);

  /* Store incoming data into rx_buffer, set rx_data_ready when a full
  * line has been received
  */
  rx_char = USART_Rx(USART1);

  if (p_rx_uart_buffer->rx_count >= DBG_RXBUFFSIZE) // avoid overrun
  {
	  return;
  }
  p_rx_uart_buffer->rx_buf[p_rx_uart_buffer->rx_tail_point] = rx_char;
  p_rx_uart_buffer->rx_tail_point = (p_rx_uart_buffer->rx_tail_point+1)%DBG_RXBUFFSIZE;

  p_rx_uart_buffer->rx_count++;

  if (p_rx_uart_buffer->rx_count > DBG_RXBUFFSIZE) // this will never happen
  {
	  p_rx_uart_buffer->rx_head_point = p_rx_uart_buffer->rx_tail_point;
  }
}
