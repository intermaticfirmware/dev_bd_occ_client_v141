//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: app.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef APP_H
#define APP_H

void AppHandler(void);
#endif
