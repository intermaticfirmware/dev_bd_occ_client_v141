//-----------------------------------------------------------------------------
//  All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: wireless.h contains constants, macros, structure definitions,
//               and public prototypes owned by the Wireless module.
//
//-----------------------------------------------------------------------------
#ifndef __WIRELESS_H__
#define __WIRELESS_H__

// Includes
//******************************************************************************
#include "native_gecko.h"
#include "bg_types.h"
#include "main.h"


// Public Definitions and Constants
//******************************************************************************
// Self provisioning and test network setup. These correspond to matching networks on the ALC1
#ifndef SELF_PROVISION_ENABLED
    #error Need definition of SELF_PROVISIONED_ENABLED. Set to 0 if not used.
#endif

// Set to 1 if you desire to publish BT-msgs ONLY on button press; set to 0 for normal operation
#define TEST_BT_ON_COMMAND  0

// External Events
//   - Host MCU UART events
#define EX_RX_HOST_MCU_UART                         ((1) << 0)
#define EX_TX_HOST_MCU_UART                         ((1) << 1)
//   - Host OCC / VACANCY events
#define EX_OCC_HOST                                 ((1) << 2)
#define EX_VAC_HOST                                 ((1) << 3)
#define EX_ONOFF_PRESSED                            ((1) << 4)
//   - Dev Board client button presses
#define EX_PB0_PRESS                                ((1) << 5)
#define EX_PB1_PRESS                                ((1) << 6)
#define EX_PB0_LONG_PRESS                           ((1) << 7)
#define EX_PB1_LONG_PRESS                           ((1) << 8)
//   - Ambient Light events
#define EX_AMBIENT_LUX                              ((1) << 7)
//   - Low battery events (active or deactive)
#define EX_LOW_BATTERY                              ((1) << 8)


// Macros - I/O definitions
//******************************************************************************

// Typedefs - Public structs, enums, etc.
//******************************************************************************

// Global Variable declarations (extern)
//******************************************************************************

// Prototype Definitions (extern)
//******************************************************************************

#endif  // __WIRELESS_H__
